from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import index

# Create your tests here.

class Story10UnitTest(TestCase):

    def home_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)